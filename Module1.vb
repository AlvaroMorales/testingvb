﻿Imports VS_TestAsync.Connection
Imports System.Threading.Tasks
Imports System.Threading

Module Module1
    Dim conn As New Connection
    Sub Main()

        Dim myDetails As New DataTable
        myDetails = conn.obtenerConsolidado()
        Dim cont As Integer = 0

        ' Create a cancellation token and cancel it.
        Dim source1 As New CancellationTokenSource()
        Dim token1 As CancellationToken = source1.Token
        source1.Cancel()
        ' Create a cancellation token for later cancellation.
        Dim source2 As New CancellationTokenSource()
        Dim token2 As CancellationToken = source2.Token

        ' Create a series of tasks that will complete, be cancelled, 
        ' timeout, or throw an exception.
        Dim tasks(myDetails.Rows.Count - 1) As Task

        For Each row As DataRow In myDetails.Rows

            tasks(cont) = Task.Run(Sub()
                                       'Dim v = conn.obtenerDetalles(CInt(row("SERVICE_ID")), cont).Rows(0).Item("RESULT")
                                       Dim v = conn.insertarCienMillonesSP().Rows.Count

                                       Console.WriteLine(v.ToString())
                                   End Sub)

            cont = cont + 1
            Console.WriteLine(cont)
        Next


        'For i As Integer = 0 To 11
        '    Select Case i Mod 4
        '     ' Task should run to completion.
        '        Case 0
        '            tasks(i) = Task.Run(Sub() Thread.Sleep(2000))
        '     ' Task should be set to canceled state.
        '        Case 1
        '            tasks(i) = Task.Run(Sub() Thread.Sleep(2000), token1)
        '        Case 2
        '            ' Task should throw an exception.
        '            tasks(i) = Task.Run(Sub()
        '                                    Throw New NotSupportedException()
        '                                End Sub)
        '        Case 3
        '            ' Task should examine cancellation token.
        '            tasks(i) = Task.Run(Sub()
        '                                    Thread.Sleep(2000)
        '                                    If token2.IsCancellationRequested Then
        '                                        token2.ThrowIfCancellationRequested()
        '                                    End If
        '                                    Thread.Sleep(500)
        '                                End Sub, token2)
        '    End Select
        'Next
        source2.Cancel()

        Try
            Task.WaitAll(tasks)
            Console.WriteLine("Got it")
        Catch ae As AggregateException
            Console.WriteLine("One or more exceptions occurred:")
            For Each ex In ae.InnerExceptions
                Console.WriteLine("   {0}: {1}", ex.GetType().Name, ex.Message)
            Next
        End Try
        Console.WriteLine()

        'For Each row As DataRow In myDetails.Rows

        '    Task.Run(Sub()
        '                 Dim v = conn.obtenerDetalles(CInt(row("SERVICE_ID")), cont).Rows(0).Item("RESULT")
        '                 Console.WriteLine(v.ToString())
        '             End Sub)

        '    'tasks(cont) = Task.Factory.StartNew(Sub()
        '    '                                        conn.obtenerDetalles(CInt(row("SERVICE_ID")))
        '    '                                    End Sub)
        '    'Task.Factory.StartNew(Sub()
        '    '                          conn.obtenerDetalles(CInt(row("SERVICE_ID")))
        '    '                      End Sub, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.FromCurrentSynchronizationContext())

        '    cont = cont + 1
        '    Console.WriteLine(cont)

        'Next
        ''Task.Factory.ContinueWhenAll(tasks, Sub(completedTasks)
        ''                                        Console.WriteLine("Ending")
        ''                                    End Sub)
        'Console.Read()
    End Sub



End Module
